// Package secretbox is all about convenience and working with Go NACL Secretbox.
// It provides helper functions to save that boilerplate you usually must do.
// Remember that Secretbox does all operations in-memory so its not suggested
// to use it with big files. Otherwise RAM / Disk might flow over.
// A unscientific rule of thumb: Files with the size of a 10th of the real RAM should work.
// NOTE: I am no Crypto specialist so i recommend not using any of this file. In worst case
// its not secure at all. Use on your own risk!
package secretbox

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	naclSecretbox "golang.org/x/crypto/nacl/secretbox"
)

const (
	// NonceSize is 24 bytes long nonce
	NonceSize = 24
	// KeySize is 32 bytes long key
	KeySize = 32
	// OpenFilePermMode is file security flag for saving opened files
	OpenFilePermMode = 0600
	// SealFilePermMode is file security flag for saving sealed files
	SealFilePermMode = 0600
)

// GenerateNoncePart generates a 24 bytes long nonce. It must be unique for every
// chunk written.
// c = number of current message part
func GenerateNoncePart(c int64) (*[NonceSize]byte, error) {
	var nonce [NonceSize]byte

	_, err := rand.Reader.Read(nonce[:])
	if err != nil {
		return nil, err
	}

	return &nonce, nil
}

// GenerateNonce same as GenerateNonceNum
func GenerateNonce() (*[NonceSize]byte, error) {
	return GenerateNoncePart(1)
}

// GenerateKey generates a crypto random 32 byte long key
func GenerateKey() (*[KeySize]byte, error) {
	var key [KeySize]byte

	_, err := rand.Reader.Read(key[:])
	if err != nil {
		return nil, err
	}

	return &key, err
}

// GenerateKeyString generates a crypto random key as string (hex format)
func GenerateKeyString() (*string, error) {
	var k *[KeySize]byte
	var err error

	if k, err = GenerateKey(); err != nil {
		return nil, err
	}

	hexed := string(hex.EncodeToString(k[:]))
	return &hexed, nil
}

// NormalizeKey converts a byte array with any size to a 32 byte sized key, using sha256
func NormalizeKey(key *[]byte) *[KeySize]byte {
	var k [KeySize]byte

	sum := sha256.Sum256(*key)
	copy(k[:], sum[:])

	return &k
}

// NormalizeKeyString converts a string with any size to a 32 byte sized key, using sha256
func NormalizeKeyString(key *string) *[KeySize]byte {
	var k [KeySize]byte

	sum := sha256.Sum256([]byte(*key))
	copy(k[:], sum[:])

	return &k
}

// Seal does a secretbox seal and prepend the nonce to the beginning of the message
// and if nonce is nil it will generate one with crypto randomness
func Seal(data []byte, nonce *[NonceSize]byte, key *[KeySize]byte) ([]byte, error) {
	// generate a nonce if necessary
	if nonce == nil {
		var err error
		nonce, err = GenerateNonce()
		if err != nil {
			return nil, err
		}
	}

	var c []byte
	c = naclSecretbox.Seal(c[:0], data, nonce, key)

	// prepend nonce to payload
	var payload = make([]byte, NonceSize+len(c))
	copy(payload[:24], nonce[:])
	copy(payload[24:], c)

	return payload, nil
}

// MustSeal is same as Seal but panics on error
func MustSeal(data []byte, nonce *[NonceSize]byte, key *[KeySize]byte) []byte {
	var out []byte
	var err error

	if out, err = Seal(data, nonce, key); err != nil {
		panic(err)
	}

	return out
}

// WriteSealedFile saves data to the target file and prepends nonce to beginning
func WriteSealedFile(filename string, data []byte, key *[KeySize]byte, perm os.FileMode) error {
	payload := MustSeal(data, nil, key)
	err := ioutil.WriteFile(filename, payload, perm)
	if err != nil {
		return err
	}
	return nil
}

// Open does a secretbox open with a prepended nonce in the payload
func Open(payload []byte, key *[KeySize]byte) ([]byte, error) {
	var nonce [NonceSize]byte
	copy(nonce[:], payload[:24])

	var data = make([]byte, len(payload)-NonceSize)
	copy(data, payload[24:])

	var out []byte
	out, ok := naclSecretbox.Open(out, data, &nonce, key)
	if !ok {
		return nil, fmt.Errorf("Secretbox open: decryption error")
	}

	return out, nil
}

// MustOpen is same as Open but panics on error
func MustOpen(payload []byte, key *[KeySize]byte) []byte {
	var out []byte
	var err error

	if out, err = Open(payload, key); err != nil {
		panic(err)
	}

	return out
}

// ReadSealedFile decrypts a file to a byte slice
func ReadSealedFile(filename string, key *[KeySize]byte) ([]byte, error) {
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	data, err := Open(file, key)
	if err != nil {
		return nil, err
	}

	return data, nil
}

// ReadSealedFileAsBuffer same as ReadSealedFile but returns a Buffer instead
func ReadSealedFileAsBuffer(filename string, key *[KeySize]byte) (*bytes.Buffer, error) {
	data, err := ReadSealedFile(filename, key)
	if err != nil {
		return nil, err
	}

	return bytes.NewBuffer(data), nil
}

// OpenFile reads a file and writes it decrypted to another file. Decryption happens
// in-memory so big files will likely crash the system.
func OpenFile(infile string, outfile string, key *[KeySize]byte) error {
	data, err := ReadSealedFile(infile, key)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(outfile, data, OpenFilePermMode)
}

// SealFile reads a file and writes it encrypted to another file. Encryption happens
// in-memory so big files will likely crash the system.
func SealFile(infile string, outfile string, key *[KeySize]byte) error {
	data, err := ioutil.ReadFile(infile)
	if err != nil {
		return err
	}

	return WriteSealedFile(outfile, data, key, SealFilePermMode)
}

// OpenCompressedFile is same as OpenFile but expects a encrypted + compressed in-file
func OpenCompressedFile(infile string, outfile string, key *[32]byte) error {
	buf, err := ReadSealedFileAsBuffer(infile, key)
	if err != nil {
		return err
	}

	gzr, _ := gzip.NewReader(buf)
	defer gzr.Close()

	tr := tar.NewReader(gzr)
	_, err = tr.Next()
	if err != nil {
		return err
	}

	// os.Remove(outfile)
	file, err := os.Create(outfile)
	if err != nil {
		return err
	}
	defer file.Close()

	io.Copy(file, tr)

	return nil
}

// SealCompressedFile is same as SealFile but also compresses the file prior encryption
func SealCompressedFile(infile string, outfile string, key *[32]byte, gzipLevel int) error {
	fileIn, err := os.Open(infile)
	if err != nil {
		return err
	}
	defer fileIn.Close()

	if gzipLevel < 0 {
		gzipLevel = gzip.BestCompression
	}

	var buf = new(bytes.Buffer)
	gzipWriter, _ := gzip.NewWriterLevel(buf, gzip.BestCompression)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	if stat, err := fileIn.Stat(); err == nil {
		// now lets create the header as needed for this file within the tarball
		header := new(tar.Header)
		header.Name = stat.Name()
		header.Size = stat.Size()
		header.Mode = int64(stat.Mode())
		header.ModTime = stat.ModTime()

		// write the header to the tarball archive
		if err := tarWriter.WriteHeader(header); err != nil {
			return err
		}
		// write file contents
		io.Copy(tarWriter, fileIn)
	}

	tarWriter.Flush()
	gzipWriter.Flush()

	// os.Remove(outfile)
	WriteSealedFile(outfile, buf.Bytes(), key, 0644)

	return nil
}
